# Trenz-Serenity_ZynqMP 

## Prerequesites
- Vivado 2018.3 installation
- XSDK 2018.3
- Petalinux 2018.3

## Step by step build how to
### Generate Hardware Project / Files
0. source vivado settings `source /opt/Xilinx/Vivado/2018.3/settings64.sh`
1. Goto vivado folder `cd vivado`
2. Start Vivado and source the project TCL `vivado -source trenz-serenity_zynqmp.tcl &`
3. Run toolflow / generate bitfile
5. Export Hardware (e.g. to standard folder)

### Updating repository
1. File -> Project -> Write TCL.. and select nothing

