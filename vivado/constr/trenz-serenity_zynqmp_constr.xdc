#set_property PACKAGE_PIN L13 [get_ports IPMC_UART_0_rxd]
#set_property PACKAGE_PIN L14 [get_ports IPMC_UART_0_txd]
#set_property IOSTANDARD LVCMOS33 [get_ports IPMC_UART_0_*]        
        
set_property PACKAGE_PIN A11 [get_ports PMOD_GPIO_0_tri_io[0]]
set_property PACKAGE_PIN A13 [get_ports PMOD_GPIO_0_tri_io[1]]
set_property PACKAGE_PIN J14 [get_ports PMOD_GPIO_0_tri_io[2]]
set_property PACKAGE_PIN E13 [get_ports PMOD_GPIO_0_tri_io[3]] 
set_property IOSTANDARD LVCMOS33 [get_ports PMOD_GPIO_0_tri_io*] 

set_property PACKAGE_PIN A12 [get_ports pmod_jtag_tck]
set_property PACKAGE_PIN B13 [get_ports pmod_jtag_tms]
set_property PACKAGE_PIN K14 [get_ports pmod_jtag_tdi]
set_property PACKAGE_PIN E14 [get_ports pmod_jtag_tdo] 
set_property IOSTANDARD LVCMOS33 [get_ports pmod_jtag*] 

set_property PACKAGE_PIN E15 [get_ports SERVICES_CMX_IIC_0_scl_io]
set_property PACKAGE_PIN F15 [get_ports SERVICES_CMX_IIC_0_sda_io]
set_property IOSTANDARD LVCMOS33 [get_ports SERVICES_CMX_IIC_0_*]

set_property PACKAGE_PIN AE5 [get_ports dp_aux_data_in_0]
set_property PACKAGE_PIN AF5 [get_ports dp_hot_plug_detect_0]
set_property PACKAGE_PIN AD4 [get_ports dp_aux_data_out_0]
set_property PACKAGE_PIN AD5 [get_ports dp_aux_data_oe_n_0]
set_property IOSTANDARD LVCMOS18 [get_ports dp_*]

set_property PACKAGE_PIN V5 [get_ports GT_DIFF_REFCLK1_0_clk_n]
set_property PACKAGE_PIN V6 [get_ports GT_DIFF_REFCLK1_0_clk_p]

set_property BITSTREAM.GENERAL.COMPRESS TRUE [current_design]
set_property BITSTREAM.CONFIG.UNUSEDPIN PULLNONE [current_design]