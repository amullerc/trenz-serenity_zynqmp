--Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2018.3 (lin64) Build 2405991 Thu Dec  6 23:36:41 MST 2018
--Date        : Tue Oct  1 17:08:43 2019
--Host        : ipe-cms-optical running 64-bit unknown
--Command     : generate_target zusys_wrapper.bd
--Design      : zusys_wrapper
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity zusys_wrapper is
  port (
    GT_DIFF_REFCLK1_0_clk_n : in STD_LOGIC;
    GT_DIFF_REFCLK1_0_clk_p : in STD_LOGIC;
    GT_SERIAL_RX_0_rxn : in STD_LOGIC_VECTOR ( 0 to 0 );
    GT_SERIAL_RX_0_rxp : in STD_LOGIC_VECTOR ( 0 to 0 );
    GT_SERIAL_TX_0_txn : out STD_LOGIC_VECTOR ( 0 to 0 );
    GT_SERIAL_TX_0_txp : out STD_LOGIC_VECTOR ( 0 to 0 );
    PMOD_GPIO_0_tri_io : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    SERVICES_CMX_IIC_0_scl_io : inout STD_LOGIC;
    SERVICES_CMX_IIC_0_sda_io : inout STD_LOGIC;
    dp_aux_data_in_0 : in STD_LOGIC;
    dp_aux_data_oe_n_0 : out STD_LOGIC;
    dp_aux_data_out_0 : out STD_LOGIC;
    dp_hot_plug_detect_0 : in STD_LOGIC;
    pmod_jtag_tck : out STD_LOGIC;
    pmod_jtag_tdi : out STD_LOGIC;
    pmod_jtag_tdo : in STD_LOGIC;
    pmod_jtag_tms : out STD_LOGIC
  );
end zusys_wrapper;

architecture STRUCTURE of zusys_wrapper is
  component zusys is
  port (
    dp_aux_data_in_0 : in STD_LOGIC;
    dp_aux_data_oe_n_0 : out STD_LOGIC;
    dp_aux_data_out_0 : out STD_LOGIC;
    dp_hot_plug_detect_0 : in STD_LOGIC;
    pmod_jtag_tdi : out STD_LOGIC;
    pmod_jtag_tms : out STD_LOGIC;
    pmod_jtag_tck : out STD_LOGIC;
    pmod_jtag_tdo : in STD_LOGIC;
    SERVICES_CMX_IIC_0_scl_i : in STD_LOGIC;
    SERVICES_CMX_IIC_0_scl_o : out STD_LOGIC;
    SERVICES_CMX_IIC_0_scl_t : out STD_LOGIC;
    SERVICES_CMX_IIC_0_sda_i : in STD_LOGIC;
    SERVICES_CMX_IIC_0_sda_o : out STD_LOGIC;
    SERVICES_CMX_IIC_0_sda_t : out STD_LOGIC;
    GT_DIFF_REFCLK1_0_clk_n : in STD_LOGIC;
    GT_DIFF_REFCLK1_0_clk_p : in STD_LOGIC;
    GT_SERIAL_TX_0_txn : out STD_LOGIC_VECTOR ( 0 to 0 );
    GT_SERIAL_TX_0_txp : out STD_LOGIC_VECTOR ( 0 to 0 );
    GT_SERIAL_RX_0_rxn : in STD_LOGIC_VECTOR ( 0 to 0 );
    GT_SERIAL_RX_0_rxp : in STD_LOGIC_VECTOR ( 0 to 0 );
    PMOD_GPIO_0_tri_i : in STD_LOGIC_VECTOR ( 3 downto 0 );
    PMOD_GPIO_0_tri_o : out STD_LOGIC_VECTOR ( 3 downto 0 );
    PMOD_GPIO_0_tri_t : out STD_LOGIC_VECTOR ( 3 downto 0 )
  );
  end component zusys;
  component IOBUF is
  port (
    I : in STD_LOGIC;
    O : out STD_LOGIC;
    T : in STD_LOGIC;
    IO : inout STD_LOGIC
  );
  end component IOBUF;
  signal PMOD_GPIO_0_tri_i_0 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal PMOD_GPIO_0_tri_i_1 : STD_LOGIC_VECTOR ( 1 to 1 );
  signal PMOD_GPIO_0_tri_i_2 : STD_LOGIC_VECTOR ( 2 to 2 );
  signal PMOD_GPIO_0_tri_i_3 : STD_LOGIC_VECTOR ( 3 to 3 );
  signal PMOD_GPIO_0_tri_io_0 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal PMOD_GPIO_0_tri_io_1 : STD_LOGIC_VECTOR ( 1 to 1 );
  signal PMOD_GPIO_0_tri_io_2 : STD_LOGIC_VECTOR ( 2 to 2 );
  signal PMOD_GPIO_0_tri_io_3 : STD_LOGIC_VECTOR ( 3 to 3 );
  signal PMOD_GPIO_0_tri_o_0 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal PMOD_GPIO_0_tri_o_1 : STD_LOGIC_VECTOR ( 1 to 1 );
  signal PMOD_GPIO_0_tri_o_2 : STD_LOGIC_VECTOR ( 2 to 2 );
  signal PMOD_GPIO_0_tri_o_3 : STD_LOGIC_VECTOR ( 3 to 3 );
  signal PMOD_GPIO_0_tri_t_0 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal PMOD_GPIO_0_tri_t_1 : STD_LOGIC_VECTOR ( 1 to 1 );
  signal PMOD_GPIO_0_tri_t_2 : STD_LOGIC_VECTOR ( 2 to 2 );
  signal PMOD_GPIO_0_tri_t_3 : STD_LOGIC_VECTOR ( 3 to 3 );
  signal SERVICES_CMX_IIC_0_scl_i : STD_LOGIC;
  signal SERVICES_CMX_IIC_0_scl_o : STD_LOGIC;
  signal SERVICES_CMX_IIC_0_scl_t : STD_LOGIC;
  signal SERVICES_CMX_IIC_0_sda_i : STD_LOGIC;
  signal SERVICES_CMX_IIC_0_sda_o : STD_LOGIC;
  signal SERVICES_CMX_IIC_0_sda_t : STD_LOGIC;
begin
PMOD_GPIO_0_tri_iobuf_0: component IOBUF
     port map (
      I => PMOD_GPIO_0_tri_o_0(0),
      IO => PMOD_GPIO_0_tri_io(0),
      O => PMOD_GPIO_0_tri_i_0(0),
      T => PMOD_GPIO_0_tri_t_0(0)
    );
PMOD_GPIO_0_tri_iobuf_1: component IOBUF
     port map (
      I => PMOD_GPIO_0_tri_o_1(1),
      IO => PMOD_GPIO_0_tri_io(1),
      O => PMOD_GPIO_0_tri_i_1(1),
      T => PMOD_GPIO_0_tri_t_1(1)
    );
PMOD_GPIO_0_tri_iobuf_2: component IOBUF
     port map (
      I => PMOD_GPIO_0_tri_o_2(2),
      IO => PMOD_GPIO_0_tri_io(2),
      O => PMOD_GPIO_0_tri_i_2(2),
      T => PMOD_GPIO_0_tri_t_2(2)
    );
PMOD_GPIO_0_tri_iobuf_3: component IOBUF
     port map (
      I => PMOD_GPIO_0_tri_o_3(3),
      IO => PMOD_GPIO_0_tri_io(3),
      O => PMOD_GPIO_0_tri_i_3(3),
      T => PMOD_GPIO_0_tri_t_3(3)
    );
SERVICES_CMX_IIC_0_scl_iobuf: component IOBUF
     port map (
      I => SERVICES_CMX_IIC_0_scl_o,
      IO => SERVICES_CMX_IIC_0_scl_io,
      O => SERVICES_CMX_IIC_0_scl_i,
      T => SERVICES_CMX_IIC_0_scl_t
    );
SERVICES_CMX_IIC_0_sda_iobuf: component IOBUF
     port map (
      I => SERVICES_CMX_IIC_0_sda_o,
      IO => SERVICES_CMX_IIC_0_sda_io,
      O => SERVICES_CMX_IIC_0_sda_i,
      T => SERVICES_CMX_IIC_0_sda_t
    );
zusys_i: component zusys
     port map (
      GT_DIFF_REFCLK1_0_clk_n => GT_DIFF_REFCLK1_0_clk_n,
      GT_DIFF_REFCLK1_0_clk_p => GT_DIFF_REFCLK1_0_clk_p,
      GT_SERIAL_RX_0_rxn(0) => GT_SERIAL_RX_0_rxn(0),
      GT_SERIAL_RX_0_rxp(0) => GT_SERIAL_RX_0_rxp(0),
      GT_SERIAL_TX_0_txn(0) => GT_SERIAL_TX_0_txn(0),
      GT_SERIAL_TX_0_txp(0) => GT_SERIAL_TX_0_txp(0),
      PMOD_GPIO_0_tri_i(3) => PMOD_GPIO_0_tri_i_3(3),
      PMOD_GPIO_0_tri_i(2) => PMOD_GPIO_0_tri_i_2(2),
      PMOD_GPIO_0_tri_i(1) => PMOD_GPIO_0_tri_i_1(1),
      PMOD_GPIO_0_tri_i(0) => PMOD_GPIO_0_tri_i_0(0),
      PMOD_GPIO_0_tri_o(3) => PMOD_GPIO_0_tri_o_3(3),
      PMOD_GPIO_0_tri_o(2) => PMOD_GPIO_0_tri_o_2(2),
      PMOD_GPIO_0_tri_o(1) => PMOD_GPIO_0_tri_o_1(1),
      PMOD_GPIO_0_tri_o(0) => PMOD_GPIO_0_tri_o_0(0),
      PMOD_GPIO_0_tri_t(3) => PMOD_GPIO_0_tri_t_3(3),
      PMOD_GPIO_0_tri_t(2) => PMOD_GPIO_0_tri_t_2(2),
      PMOD_GPIO_0_tri_t(1) => PMOD_GPIO_0_tri_t_1(1),
      PMOD_GPIO_0_tri_t(0) => PMOD_GPIO_0_tri_t_0(0),
      SERVICES_CMX_IIC_0_scl_i => SERVICES_CMX_IIC_0_scl_i,
      SERVICES_CMX_IIC_0_scl_o => SERVICES_CMX_IIC_0_scl_o,
      SERVICES_CMX_IIC_0_scl_t => SERVICES_CMX_IIC_0_scl_t,
      SERVICES_CMX_IIC_0_sda_i => SERVICES_CMX_IIC_0_sda_i,
      SERVICES_CMX_IIC_0_sda_o => SERVICES_CMX_IIC_0_sda_o,
      SERVICES_CMX_IIC_0_sda_t => SERVICES_CMX_IIC_0_sda_t,
      dp_aux_data_in_0 => dp_aux_data_in_0,
      dp_aux_data_oe_n_0 => dp_aux_data_oe_n_0,
      dp_aux_data_out_0 => dp_aux_data_out_0,
      dp_hot_plug_detect_0 => dp_hot_plug_detect_0,
      pmod_jtag_tck => pmod_jtag_tck,
      pmod_jtag_tdi => pmod_jtag_tdi,
      pmod_jtag_tdo => pmod_jtag_tdo,
      pmod_jtag_tms => pmod_jtag_tms
    );
end STRUCTURE;
